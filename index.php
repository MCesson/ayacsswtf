<?php
$page_title = "Home";
include('includes/header.php');
 ?>

  <main class="px-3 py-3">
    <h1>And Yet Another CyberSec Student Website, <span class="orangeColor">Too Fantastic</span></h1>
    <p class="lead"><i>Don't know yet what I'll talk about...</i></p>
    <p class="lead">
      <a href="contact.php" class="btn btn-lg btn-secondary fw-bold border-white bg-white">Help me to choose</a>
    </p>
  </main>

<?php
include('includes/footer.html');
 ?>
