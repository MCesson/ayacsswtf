<!DOCTYPE html>
<?php
  /* define a default title for the current page if none is provided:
  from the filename, remove characters '_' and uppercase first letter of each word */
  if (!isset($page_title)) {
    $page_title = ucwords(
      str_replace('_', ' ', pathinfo($_SERVER['SCRIPT_FILENAME'])['filename'])
    );
  }
?>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page_title; ?></title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet">

  </head>

  <body class="d-flex h-100 text-center text-white bg-dark" onload="updateBodyShadow()" onresize="updateBodyShadow()">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="mb-auto">
        <div>
          <h3 class="float-md-start mb-0">AYACSSWTF</h3>
          <nav class="nav nav-masthead justify-content-center float-md-end">
            <a class="nav-link" href="index.php">Home</a>
            <a class="nav-link" href="about.php">About</a>
            <a class="nav-link" href="portfolio.php">Portfolio</a>
            <a class="nav-link" href="contact.php">Contact</a>
          </nav>
        </div>
      </header>
