<?php
include('includes/header.php');
?>

  <main class="px-3 pb-3 article">

    <?php
      if (isset($_GET['name'])) {

        switch ($_GET['name']) {

          case 'revenge_rat_part1':
            include('contents/revenge_rat_part1.html');
            ?>
            <div class="p-3 text-center">
              <a href="portfolio.php?name=revenge_rat_part2" class="btn btn-primary border-white bg-dark" >Part 2</a>
            </div>
            <?php
            break;

          case 'revenge_rat_part2':
            include('contents/revenge_rat_part2.html');
            break;

          case 'jigsaw_part1':
            include('contents/jigsaw_part1.html');
            ?>
            <div class="p-3 text-center">
              <a href="portfolio.php?name=jigsaw_part2" class="btn btn-primary border-white bg-dark" >Part 2</a>
            </div>
            <?php
            break;

          case 'jigsaw_part2':
            include('contents/jigsaw_part2.html');
            break;

          default:
            echo '<p>Content ' . $_GET['name'] . ' not (yet) available!</p>';
            break;
        }

      } else {
        ?>
        <p style="text-align:center"><i><b>Portfolio still in development</b></i> 🔧</p><br/>
        <p style="text-indent:0">In the meantime, you can check:</p>
        <ul>
          <li>
           either my <a href="https://gitlab.com/MCesson">GitLab user account</a> for a first glance to some projects
         </li>
         <li>
           either <a href="portfolio.php?name=revenge_rat_part1">a malware analysis report I've written recently</a> for some IT Security stuff
         </li>
         <li>
           either <a href="portfolio.php?name=jigsaw_part1">another malware analysis report</a>, even more recent, for even more IT Security stuff
         </li>
        <?php
      }
     ?>

  </main>

<?php
include('includes/footer.html');
?>
