function updateBodyShadow() {
  /* Esthetic purpose regarding the shadowing frame: add 'h-100' among classes
  of tag 'body' whenever content height (document.body) is smaller than window height */

  var bodyClasses = document.getElementsByTagName('body')[0].classList;
  if (bodyClasses.contains('h-100')) { bodyClasses.remove('h-100'); }

  // console.log("Window:", window.innerHeight, "Document:", document.body.clientHeight);
  if (document.body.clientHeight < window.innerHeight){ bodyClasses.add('h-100'); }
}


function styleLinkCurrentPage() {
  /* Esthetic purpose regarding the set of navigation links: change display of the link
  referencing the current page (if the current page is referenced in the navigation links) */

  var navbarLinks = document.getElementsByTagName('nav')[0].children;

  if (document.URL.endsWith('/')) { // match URL finishing by '/' and styling of 'index.php'
    var isIndexPageURL = true;
  }

  for (var i=0; i<navbarLinks.length; i++) {
    var link = navbarLinks[i];

    if (link.href == document.URL || isIndexPageURL && link.href.endsWith('index.php')) {
      link.classList.add('active');
      link.setAttribute('aria-current', 'page');
      //'aria-current' is used to inform the assistive technology user what has been indicated via styling
      break;
    }
  }
}
