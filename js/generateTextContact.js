let initialText = "";
const generateButton = document.getElementById('generate-button');
const restoreButton = document.getElementById('restore-button');
const messageArea = document.getElementById('message');

messageArea.addEventListener('input', initiateButtons);


function initiateButtons() {
  /* Display initial 'Generate' button only after 'n' caracters inputted. */

  let n = 20;

  if (restoreButton.hidden) {

    if (generateButton.hidden) { // & message is short

      if (messageArea.value.length >= n) { // make 'Generate' button appear
        generateButton.hidden = false;
      }
    } else { // 'Generate' button is visible & message is long

      if (messageArea.value.length < n) { // make 'Generate' button disappear
        generateButton.hidden = true;
      }
    }
  }
}


function switchButtons() {
  /* Hide one button and reveal the other.
  One generates text from what user has already inputted.
  The other restores initial user text from AI generated text. */

  generateButton.hidden = !generateButton.hidden;
  restoreButton.hidden = !restoreButton.hidden;
}


function generateText() {
  /* Using Fetch API to communicate with the server, end the message
  currently inputted in the contact form with what is predicted by a NLP
  model based on the GPT-2 model of OpenAI (DeepAI Text Generation API) */

  initialText = messageArea.value;

  const formData = new FormData();
  formData.append('data', messageArea.value);

  fetch('api/text_generation.php', {
    method: 'POST',
    body: formData,
  })
  .then(response => {
    if (!response.ok) { // deal with HTTP status error codes
      throw new Error('HTTP error ' + response.status);
    }
    return response.json();
  })
  .then(json => {
    if (!json.data) { // deal with errors thrown by the fetched page
      let errorStatus = json.err ? json.err : '';
      throw new Error('fetched page returns \'' + errorStatus + '\'');
    }
    messageArea.value = json.data;
    switchButtons();
  })
  .catch(err => console.error(err)); // deal with error during fetch operation
}


function restoreText() {
  /* Display back the initial user text - generated text is then lost */

  let alertText = "Message used before text generation is about to overwrite the current one. You won't be able to get it back.\nDo you still want to proceed?";

  if (confirm(alertText)) {
    messageArea.value = initialText;
    switchButtons();
  }
}
