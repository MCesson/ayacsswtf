<?php
/* The communication with the DeepAI API is simply based on cURL.
The documentation suggests the following command to reach the API:
curl \
  -F 'text=YOUR_TEXT_URL' \
  -H 'api-key:YOUR_KEY' \
  https://api.deepai.org/api/text-generator
*/

if (isset($_POST['data'])) {

  require_once('../../deepai_api_config.php');

  // process user input
  $data = 'text=' . $_POST['data'];

  // initiate, custom and send the HTTP request
  $curl = curl_init(DEEPAI_API_URL);

  // ask for a return value to request
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  // indicate a request of type POST
  curl_setopt($curl, CURLOPT_POST, true);
  // set a custom header for authentication
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Api-Key:' . DEEPAI_API_KEY));
  // finally add the data
  curl_setopt($curl, CURLOPT_POSTFIELDS,  $data);

  $output_json = curl_exec($curl);
  curl_close($curl);

  // process API data
  $output = json_decode($output_json);

  if (property_exists($output, 'output')) {
    $new['data'] = $output->{'output'};
  } else {
    // handle error from the API
    $new['err'] = 'API answer: ' . $output_json;
  }

} else {

  // handle bad requests to the current page
  $new['err'] = 'No POST data received';
}

/* Return some values:
If it exists, the field 'data' contains data processed by the API
If it exists, the field 'err' contains error messages thrown in this file */
echo json_encode($new);
?>
