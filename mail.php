<?php
$page_title = "Sending Mail";
include('includes/header.php');
 ?>

  <main class="px-3">

    <?php
    if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['message'])) {

      echo '<p>Please fill out all necessary fields.</p>';

    } else {

      $msg = $_POST['name'] . ' <' . $_POST['email'] . "> sent you this email via \"AYACSSWTF\".\n\n";
      if (!empty($_POST['subject'])) {
          $msg = $msg . "Object: " . $_POST['subject'] . "\n";
      }
      $msg = $msg . "Message:\n" . $_POST['message'];
      $msg = wordwrap($msg,70); // 70 characters per line max to increase readability

      require_once('../mail_config.php');
      mail(MY_EMAIL, "Message from AYACSSWTF", $msg);

      echo "<p>Your mail has been sent! Thank you 😊</p>";
      header('Refresh:5; url=.');
    }
    ?>

  </main>

<?php
include('includes/footer.html');
 ?>
